### Installation and Configuration

** 1. sudo git clone https://gitlab.com/riasingh13597/prackt.git

** 2. composer update

** 3. set .env file 
        - add DB name, user and pass

** 4. install elastic search from https://www.elastic.co/guide/en/elasticsearch/reference/current/install-elasticsearch.html     

** 5. For publist asset and other dependencies from package <br>
        php artisan vendor:publish --all

** 6. php artisan key:generate

** 7. php artisan optimize:clear

** 8. composer dump-autoload

** 9: php artisan migrate

** 10: run project using php artisan serve <br> 
        Note: Plese run this project on port 8000

** 11: start elasticSearch Service <br>
        sudo service elasticsearch start

** 12: run php artisan packt:book:index --total=1000 <br>
        It is used to fetch book data from API and index data in elasticsearch