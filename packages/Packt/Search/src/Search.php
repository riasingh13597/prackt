<?php

namespace Packt\Search;

use Illuminate\Support\Facades\Config;
use Elastic\Elasticsearch\ClientBuilder;
use DB;
use Packt\Books\Models\Book;

/**
 * Class search.
 *
 */

const INDEX_NAME = 'packt_index';

class Search
{

    public function ElasticSearchIndex()
    {
        $response = Book::get()->toArray();
        $params = ['body' => []];
        foreach ($response as $product) {

            $params['body'][] = [
                "index" => [
                    '_index' => self::INDEX_NAME,
                    '_id' => $product['id']
                ]
            ];

            $params['body'][] = $product;
        }

        $client =  ClientBuilder::create()->build();
        $client->bulk($params);


        return $response = array("status" => "1", "msg" => "Data indexed in ElasticSearch");
    }

    public function updateDataInElastic($book)
    {
        $book = $book->toArray();
        $data = [
            'body' =>
            [
                'doc' =>
                $book

            ],
            'index' => self::INDEX_NAME,
            'id'    => $book['id']
        ];

        $client =  ClientBuilder::create()->build();
        $client->update($data);
    }

    public function deleteDataInElastic($id)
    {
        $data = [
            'index' => self::INDEX_NAME,
            'id'    => $id
        ];

        $client =  ClientBuilder::create()->build();
        $client->delete($data);
    }

    public function createDataInElastic($book)
    {
        $book = $book->toArray();
        $data = [
            'body' => $book,
            'index' => self::INDEX_NAME,
            '_id'    => $book['id']
        ];

        $client =  ClientBuilder::create()->build();
        $client->index($data);
    }
    public function autoComplete($requestData)
    {
        $searchData = $requestData['search_data'] ?? "";
        $reqData['query']['match_phrase_prefix']['title'] = $searchData;
        $reqData['from'] = '0';
        $reqData['size'] = '10';
        $searchData = $this->curlCall(json_encode($reqData));
        $response = (array)($searchData);
       
        if(!empty($response)){
            $response = (array)$response['hits'];
            $response = (array) $response['hits'];
        }
        return $response;
    }

    public function search($requestData)
    {
        $searchData = $requestData['search_data'] ?? "";
        $filters = $requestData['filter'] ?? "";
        $author = [];
        $publisher = [];
        $genre = [];
        $filterSearch = [];
        if (!empty($filters)) {
            foreach ($filters as $filter) {
                $temp = (explode("_", $filter));
                $newTemp[$temp['0']] = $temp['1'];
                $data = (object)$newTemp;
                $newdata[]['match'] = (object)$data;
                $newTemp = [];
            }

            $filterSearch['bool']['should'] = json_decode(json_encode((array)($newdata)));
        }

        $nameSearch['match']['title'] = $searchData;
        if (!empty($filterSearch)) {
            $mustQuery = array($nameSearch, $filterSearch);
        } else {
            $mustQuery = array($nameSearch);
        }


        $reqData['query']['bool']['must'] = $mustQuery;

        $searchData = $this->curlCall(json_encode($reqData));
        $response = (array)($searchData);
        if(!empty($response)){
            $response = (array)$response['hits'];
        
            $total = $response['total']->value;
            $response = (array) $response['hits'];
            
           
            $response = array_column((array)$response, '_source');
            foreach ($response as $temp) {
                $author[] = $temp->author;
                $publisher[] = $temp->publisher;
                $genre[] = $temp->genre;
            }
        }
       
        $filter = ['author' => $author, 'publisher' => $publisher, 'genre' => $genre];
        $response = array('response' => $response, 'filter' => $filter, 'selected_filter' => $filters);
        return $response;
    }

   

    public function curlCall($reqData)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://localhost:9200/packt_index/_search',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_POSTFIELDS => $reqData,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return json_decode($response);
    }
}
