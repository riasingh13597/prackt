<?php

namespace Packt\Search\Console\Commands;

use Illuminate\Console\Command;
use Packt\Books\Models\Book;
use Packt\Search\Facades\Search;
class DataFeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'packt:elastic:index';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatically Index data in Elastic';

    /**
     * CatalogRuleIndex object
     *
     * @var \Sultan\CatalogRule\Helpers\CatalogRuleIndex
    */
    

    /**
     * Create a new command instance.
     *
     * @param  \Sultan\CatalogRuleProduct\Helpers\CatalogRuleIndex  $catalogRuleIndexHelper
     * @return void
     */
    

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        print_r(Search::ElasticSearchIndex());
        
    }
}