<?php
Route::get('/search', 'Packt\Search\Http\Controllers\Search\SearchController@search');
Route::get('/autocomplete', 'Packt\Search\Http\Controllers\Search\SearchController@autoComplete');