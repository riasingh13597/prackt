<?php

namespace Packt\Search\Http\Controllers\Search;

use App\Http\Controllers\Controller;
use Request;
use Packt\Search\Facades\Search;

class SearchController extends Controller
{
    public function Search(Request $request)
    {
        $request = request()->all();
        $searchResponse = Search::search($request);
        return ($searchResponse);
    }

    public function autoComplete(Request $request){
        $request = request()->all();
        $autocomplete = [];
        $searchResponses = Search::autoComplete($request);
        foreach($searchResponses as $searchResponse ){
            $searchResponse = (array)$searchResponse;
            $data = (array)$searchResponse['_source'];
            $autocomplete[] = $data['title'];
        }
            
        $response['terms'] = $autocomplete; 
        
        return ($response);
    }
}