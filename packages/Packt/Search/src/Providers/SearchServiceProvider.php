<?php

namespace Packt\Search\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Event;
use Illuminate\Routing\Router;
use Illuminate\Foundation\AliasLoader;
use Packt\Search\Console\Commands\DataFeed;
use Packt\Search\Search;
class SearchServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        $this->loadRoutesFrom(__DIR__.'/../Http/routes.php');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerCommands();
        $this->registerFacades();
    }

   
    /**
     * Register the console commands of this package
     */
    protected function registerCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                DataFeed::class,
            ]);
            
        }
    }

    protected function registerFacades()
    {
        $loader = AliasLoader::getInstance();
        $loader->alias('search', SearchFacade::class);

        $this->app->singleton('search', function () {
            return new Search();
        });

        $this->registerCommands();
    }


}