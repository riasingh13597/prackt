<?php

namespace Packt\Books\Console\Commands;

use Illuminate\Console\Command;
use Packt\Books\Models\Book;
use Packt\Search\Facades\Search;
class DataFeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'packt:book:index {--total=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatically Index data in Book Tables and elasticsearch. You can fetch any no of data using  --total=1000.';

    /**
     * CatalogRuleIndex object
     *
     * @var \Sultan\CatalogRule\Helpers\CatalogRuleIndex
    */
    

    /**
     * Create a new command instance.
     *
     * @param  \Sultan\CatalogRuleProduct\Helpers\CatalogRuleIndex  $catalogRuleIndexHelper
     * @return void
     */
    

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $total = $this->options('total');
        $ch = curl_init('https://fakerapi.it/api/v1/books?_quantity='.$total['total'] );
 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        curl_close($ch);
          
        $Datas = json_decode($data, true);
      
        foreach ($Datas['data'] as $data) {
            Book::create($data);
        }

       search::ElasticSearchIndex();
    }
}