<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add Book Form </title>
 
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" >
</head>
<body>
 
<div class="container mt-2">
   
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left mb-2">
            <h2>Add Book</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('book.index') }}"> Back</a>
        </div>
    </div>
</div>
    
  @if(session('status'))
    <div class="alert alert-success mb-1 mt-1">
        {{ session('status') }}
    </div>
  @endif
    
<form action="{{ route('book.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
   
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Book Name:</strong>
                <input required type="text" name="title" class="form-control" placeholder="Book Name">
               
            </div>
        </div>
 
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Book Genre:</strong>
                 <input required type="text" name="genre" class="form-control" placeholder="Company Email">
                
            </div>
        </div>
 
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Book Author:</strong>
                 <input required type="text" name="author" class="form-control" placeholder="Author">
               
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Book Description:</strong>
                 <input required type="text" name="description" class="form-control" placeholder="Description">
                
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Book Isbn:</strong>
                 <input required type="text" name="isbn" class="form-control" placeholder="Isbn">
              
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Book Image:</strong>
                 <input required type="text" name="image" class="form-control" placeholder="Image">
               
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Book Published:</strong>
                <input type="date"  name="published" class="form-control" placeholder="Date" >
               
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Book Publisher:</strong>
                 <input required type="text" name="publisher" class="form-control" placeholder="Publisher">
                
            </div>
        </div>

        
        <button type="submit" class="btn btn-primary ml-3">Submit</button>
    </div>
    
</form>
 
</body>
</html>
