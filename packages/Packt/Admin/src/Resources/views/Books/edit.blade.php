<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit Book Form </title>
 
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" >
</head>
<body>
 
<div class="container mt-2">
 
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Book</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('book.index') }}" enctype="multipart/form-data"> Back</a>
            </div>
        </div>
    </div>
    
  @if(session('status'))
    <div class="alert alert-success mb-1 mt-1">
        {{ session('status') }}
    </div>
  @endif
   
    <form action="{{ route('book.update',$book->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
    
        <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Book Name:</strong>
                <input type="text" name="title" class="form-control" value="{{ $book->title }}" placeholder="Book Name">
               
            </div>
        </div>
 
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Book Genre:</strong>
                 <input type="text" name="genre" class="form-control" value="{{ $book->genre }}" placeholder="Company Email">
                
            </div>
        </div>
 
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Book Author:</strong>
                 <input type="text" name="author" class="form-control" value="{{ $book->author }}" placeholder="Author">
               
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Book Description:</strong>
                 <input type="text" name="description" class="form-control" value="{{ $book->description }}" placeholder="Description">
                
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Book Isbn:</strong>
                 <input type="text" name="isbn" class="form-control" value="{{ $book->isbn }}" placeholder="Isbn">
              
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Book Image:</strong>
                 <input type="text" name="image" class="form-control" value="{{ $book->image }}" placeholder="Image">
               
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Book Published:</strong>
                 <input type="date"  name="published" class="form-control" value="{{ $book->published }}"  min="2018-01-01" max="2018-12-31">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Book Publisher:</strong>
                 <input type="text" name="publisher" class="form-control" value="{{ $book->publisher }}" placeholder="Publisher">
                
            </div>
        </div>
              <button type="submit" class="btn btn-primary ml-3">Submit</button>
           
        </div>
    
    </form>
</div>
 
</body>
</html>
