<?php

namespace Packt\Admin\Http\Controllers\Books;

use App\Http\Controllers\Controller;
use Request;
use Packt\Books\Models\Book;
use Packt\Search\Facades\Search;
class BookController extends Controller
{
    public function index()
    {
      $data['books'] = Book::orderBy('id','asc')->paginate(10);
    
      return view('admin::Books.index', $data);
    }

    public function create()
    {
      return view('admin::Books.create');
    }

    public function store(Request $request)
    {
      
      $request = request()->all();
      $book = new Book;

      $book->title = $request['title'];
      $book->author = $request['author'];
      $book->genre = $request['genre'];
      $book->description = $request['description'];
      $book->isbn = $request['isbn'];
      $book->image = $request['image'];
      $book->published = $request['published'];
      $book->publisher = $request['publisher'];
     
      $book->save();
      
      search::createDataInElastic($book);
  
      return redirect()->route('book.index')
                    ->with('success','Book has been created successfully.');
    }

    public function show(Book $book){
      return view('admin::Books.show' ,compact('book'));
    }

    public function edit($id)
    {
      $book =  Book::find($id);
       return view('admin::Books.edit',compact('book'));
    }

    public function update(Request $request, $id)
    {
      
      $request = request()->all();
      $book = Book::find($id);

      $book->title = $request['title'];
      $book->author = $request['author'];
      $book->genre = $request['genre'];
      $book->description = $request['description'];
      $book->isbn = $request['isbn'];
      $book->image = $request['image'];
      $book->published = $request['published'];
      $book->publisher = $request['publisher'];

      $book->save();
    
      search::updateDataInElastic($book);
      return redirect()->route('book.index')
                    ->with('success','Book Has Been updated successfully');
    }

    public function destroy($id)
    {
      $book =  Book::find($id);
      $book->delete();
      search::deleteDataInElastic($id);
      return redirect()->route('book.index')
                      ->with('success','Book has been deleted successfully');
  }
}

  