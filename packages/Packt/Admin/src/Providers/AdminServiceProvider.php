<?php

namespace Packt\Admin\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Event;
use Illuminate\Routing\Router;
use Illuminate\Foundation\AliasLoader;

class AdminServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        $this->loadRoutesFrom(__DIR__ . '/../Http/routes.php');
        $this->loadViewsFrom(__DIR__ . '/../Resources/views', 'admin');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // /$this->registerFacades();
    }

    protected function registerFacades()
    {
        $loader = AliasLoader::getInstance();
        $loader->alias('search', SearchFacade::class);

        $this->app->singleton('search', function () {
            return new Search();
        });

        $this->registerCommands();
    }

    /**
     * Register the console commands of this package
     */
    protected function registerCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                SearchIndex::class,
                GenearteSchema::class,
                ElasticSearchIndex::class,
                GeneareElasticSearchSchema::class
            ]);
            
        }
    }

}